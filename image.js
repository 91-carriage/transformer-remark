const visit = require('unist-util-visit')

const absoluteRE = /^(\/|https?:\/\/)/i;

function imageProcessor(options = {}) {
  const transformer = this.data('transformer')

  return async function transform (tree, file, callback) {
    if (!transformer) return callback()
    if (!file.path) return callback()

    const images = []

    visit(tree, 'image', node => images.push(node))

    for (const node of images) {
      const data = node.data || {}
      const props = data.hProperties || {}
      const classNames = props.class || []

      let path = file.data.node
        ? transformer.resolveNodeFilePath(file.data.node, node.url)
        : node.url

      if (path === node.url && file.data.node && !node.url.match(absoluteRE)) {
        const pathAttempt = `./${node.url}`;
        const newPath = transformer.resolveNodeFilePath(file.data.node, pathAttempt);
        if (newPath !== pathAttempt) {
          path = newPath;
        }
      }

      let imageHTML = null
      let noscriptHTML = null

      try {
        const asset = await transformer.assets.add(path, {
          alt: props.alt || node.alt,
          title: props.title || node.title,
          width: props.width,
          height: props.height,
          classNames,
          ...options
        })

        imageHTML = asset.imageHTML
        noscriptHTML = asset.noscriptHTML
      } catch (err) {
        callback(err)
        return
      }

      if (imageHTML) {
        node.type = 'html'
        node.value = imageHTML + noscriptHTML
      }
    }

    callback()
  }
}

module.exports = imageProcessor;
