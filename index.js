const BaseTransformer = require('@gridsome/transformer-remark');
const imageProcessor = require('./image');

const absoluteRE = /^(\/|https?:\/\/|\.)/i;

class RemarkTransformer extends BaseTransformer {
  parse (source) {
    const res = super.parse(source);

    this.options.makeRelative.forEach(( option ) => {
      if (option in res) {
        if (!res[option] || res[option].match(absoluteRE)) {
          return;
        }

        res[option] = `./${res[option]}`;
      }
    });

    return res;
  }

  createProcessor (options = {}) {
    if (this.options.processImages !== false) {
      const plugins = this.options.plugins || [];

      plugins.push([imageProcessor, {
        blur: options.imageBlurRatio,
        quality: options.imageQuality,
        background: options.imageBackground,
        immediate: options.lazyLoadImages === false ? true : undefined
      }]);

      this.options.processImages = false;
      this.options.plugins = plugins;
    }

    return super.createProcessor(options);
  }
}

module.exports = RemarkTransformer;
